namespace MyEventsUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedRequiredFieldInEvent : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Events", "EventTitle", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Events", "EventTitle", c => c.String());
        }
    }
}
