﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyEventsUI.Startup))]
namespace MyEventsUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
