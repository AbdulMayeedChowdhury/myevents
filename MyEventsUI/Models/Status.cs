﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyEventsUI.Models
{
    public class Status
    {
        public int StatusId { get; set; }
        public string StatusTitle { get; set; }
    }
}