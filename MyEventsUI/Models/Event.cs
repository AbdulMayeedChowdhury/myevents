﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyEventsUI.Models
{
    public class Event
    {
        [Required]
        public int EventId { get; set; }
        [Required]
        public string EventTitle { get; set; }
        public string Description { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public TimeSpan StartTime { get; set; }
        [Required]
        public TimeSpan EndTime { get; set; }
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }
    }
}